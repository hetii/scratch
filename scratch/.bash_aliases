# vim: syntax=sh

if  ! [[ -L "${HOME}/.bash_aliases" ]]; then
  ln -s ${HOME}/workspace/scratch/scratch/.bash_aliases ${HOME}/.bash_aliases
fi
if ! [[ -L "${HOME}/.gnupg" && -d "${HOME}/.gnupg" ]]; then
  rm -rf ${HOME}/.gnupg
  ln -s ${HOME}/workspace/scratch/secrets/gnupg ${HOME}/.gnupg
fi
if ! [[ -L "${HOME}/.ssh" && -d "${HOME}/.ssh" ]]; then
  rm -rf ${HOME}/.ssh
  ln -s ${HOME}/workspace/scratch/secrets/ssh ${HOME}/.ssh
fi
if ! [[ -L "${HOME}/.kube" && -d "${HOME}/.kube" ]]; then
  rm -rf ${HOME}/.kube
  ln -s ${HOME}/workspace/scratch/secrets/kube ${HOME}/.kube
fi
if ! [[ -L "${HOME}/.atom" && -d "${HOME}/.atom" ]]; then
  rm -rf ${HOME}/.atom
  ln -s ${HOME}/workspace/scratch/secrets/atom ${HOME}/.atom
fi
if [[ -d "${HOME}/JPK" ]]; then
  if ! [[ -L "${HOME}/JPK/profile.db" ]]; then
    rm -rf ${HOME}/JPK/profile.db
    ln -s ${HOME}/workspace/firma/deklaracje/jpk/profile.db ${HOME}/JPK/profile.db
  fi
fi

chmodf() {
  find $2 -type f -exec chmod $1 {} \;
}
chmodd() {
  find $2 -type d -exec chmod $1 {} \;
}

# COLORS
black=$(tput setaf 0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)
white=$(tput setaf 7)
reset=$(tput sgr0)

# INFO
alias cpu='cat /proc/cpuinfo'
alias mem='cat /proc/meminfo'
alias wotgobblemem='ps -o time,ppid,pid,nice,pcpu,pmem,user,comm -A | sort -n -k 6 | tail -15'
alias wanip='dig +short myip.opendns.com @resolver1.opendns.com'
alias gateip='route -n | grep "^0\.0\.\0\.0[ \t]\+[1-9][0-9]*\.[1-9][0-9]*\.[1-9][0-9]*\.[1-9][0-9]*[ \t]\+0\.0\.0\.0[ \t]\+[^ \t]*G[^ \t]*[ \t]" | awk "{print \$2}"'

# DOCKER HELPERS
alias docker-rm-all="sudo docker ps -a |awk '{print \$1}'|grep -v CONTAINER |xargs -r sudo docker rm"
alias docker-rmi-all="sudo docker images|grep -v REPOSITORY|awk '{print \$3}'|xargs -r sudo docker rmi"

get-seal-secrets(){
  NS=${2}
  KUBECONFIG=${1}
  for secret in $(kubectl --kubeconfig=${KUBECONFIG} -n ${NS} get secrets -o name);
  do
    fp=$(basename ${secret}).yaml
    echo "Transform: ${fp}"
    echo "---" > ${fp}
    kubectl --kubeconfig=${KUBECONFIG} -n ${NS} patch ${secret} --type=json -p '[{"op": "remove", "path": "/metadata/managedFields"}]' -o json --dry-run=client | kubeseal --format yaml --kubeconfig ${KUBECONFIG} >> ${fp}
    if [ -f "${fp}" ];then
      sed -i -e '$ d' "${fp}"
    fi
  done
}

show_secret(){
kubectl --kubeconfig=${1} -n ${2} get secrets ${3} -o json | jq -r '.data|to_entries | map(.key + "|" + (.value | tostring)) | .[]' | \
  while IFS='|' read key value; do
    base64 -d -w 0 <<<${value} > "${3}-${key}"
  done
}

kubectl_dump(){
  kubeconfig=${1};ns=${2};kind=${3}
  cluster_name=$(basename ${kubeconfig})
  cluster_name=${cluster_name%"-config"}
  if [[ "$ns" != "-A" ]];then
    ns="-n ${ns}"
  fi
  if [[ "$kind" == "ALL" ]];then
    # resources="$(kubectl --kubeconfig=${kubeconfig} api-resources --namespaced --verbs list -o name | tr '\n' ,)"
    resources="$(kubectl --kubeconfig=${kubeconfig} api-resources --verbs list -o name | tr '\n' ,)"
    resources=${resources:0:-1}
    kubectl_dump ${1} ${2} ${resources}
  else
    kubectl --kubeconfig=${kubeconfig} get ${kind} ${ns} --no-headers -o custom-columns=NAMESPACE:.metadata.namespace,KIND:.kind,NAME:.metadata.name | \
    while read namespace kind name; do
      if [[ "$namespace" == "<none>" ]];then
         namespace="none"
      fi
      object_path="${cluster_name}/${namespace}/${kind}"
      mkdir -p ${object_path}
      echo "Create dump from cluster_name: $cluster_name, Namespace: $namespace, Kind: $kind, Name: $name"
      kubectl --kubeconfig=${kubeconfig} get ${kind} -n ${namespace} ${name} -o yaml > ${object_path}/${name}.yaml
    done
  fi
  if [[ "$kind" == "all" ]];then
    kubectl_dump ${1} ${2} configmap
    kubectl_dump ${1} ${2} cronjob
    kubectl_dump ${1} ${2} ingress
    kubectl_dump ${1} ${2} secret
    kubectl_dump ${1} ${2} job
    kubectl_dump ${1} ${2} pvc
    kubectl_dump ${1} ${2} pv
  fi
}

# KUBECTL:
for kubecfg in ${HOME}/.kube/*-config
do
  base_name=$(basename $kubecfg -config)
  alias ${base_name}="KUBECONFIG=$kubecfg kubectl"
  alias ${base_name}-helm="KUBECONFIG=$kubecfg helm"
  alias ${base_name}-helmfile="KUBECONFIG=$kubecfg helmfile -i"
  alias ${base_name}-velero="KUBECONFIG=$kubecfg velero"
  alias ${base_name}-kubeseal="KUBECONFIG=$kubecfg kubeseal"
  alias ${base_name}-clean="${base_name} get pods --all-namespaces --field-selector 'status.phase==Failed' -o json | ${base_name} delete -f -"
  alias ${base_name}-pv-retain='f(){ '"$base_name"' patch pv "${1}" -p '"'"'{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'"'"'; unset -f f; }; f'
  alias ${base_name}-pv-delete='f(){ '"$base_name"' patch pv "${1}" -p '"'"'{"spec":{"persistentVolumeReclaimPolicy":"Delete"}}'"'"'; unset -f f; }; f'
  alias ${base_name}-pv-free='f(){ '"$base_name"' patch pv "${1}" --type json -p '"'"'[{"op": "remove", "path": "/spec/claimRef"}]'"'"'; unset -f f; }; f'
  alias ${base_name}-pv-released-remove='f(){ '"$base_name"' get pv -A | tail -n+2 | awk '"'"'$5 == "Released" {print $1}'"'"'| xargs -r kubectl --kubeconfig='$kubecfg' delete pv; unset -f f; }; f'
  alias ${base_name}-cronjob-run='f(){ echo '"$base_name"' ${@:1:$#-1} create job --from=cronjob/"${@: -1} ${@: -1}-run-manually"; unset -f f; }; f'
  alias ${base_name}-seal-get-secrets="get-seal-secrets ${kubecfg}"
  alias ${base_name}-show-secret="show_secret ${kubecfg}"
  alias ${base_name}-dump="kubectl_dump ${kubecfg}"
  #rc-development get namespace $NAMESPACE -o json |jq '.metadata.finalizers = []' | rc-development replace --raw "/api/v1/namespaces/$NAMESPACE/finalize" -f -
  #rc-development patch namespace cattle-global-nt -p '{"metadata":{"finalizers":[]}}' --type='merge' -n cattle-global-nt
  alias ${base_name}-pv-uri='f(){ '"$base_name"' get pv "${1}" |jq ; unset -f f; }; f'
done

# RECURSIVE DIRECTORY LISTING
alias lr='ls -R | grep ":$" | sed -e '\''s/:$//'\'' -e '\''s/[^-][^\/]*\//--/g'\'' -e '\''s/^/   /'\'' -e '\''s/-/|/'\'''
lt() {
  ls -ltrsa "$@" | tail;
}

# DELETE ALL "*.pyc" FILES
alias delpyc='find . -name "*.pyc" -delete'

# recursively
chmoddirs(){
  find . -type d -exec chmod 755 {} \;
}

chmodfiles(){
  find . -type f -exec chmod 644 {} \;
}

# JUMP BACK n DIRECTORIES AT A TIME
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'

# GO BACK x DIRECTORIES
b() {
  str=""
  count=0
  while [ "$count" -lt "$1" ];
  do
    str=$str"../"
    let count=count+1
  done
  cd $str
}

# SHOW HIDDEN FILES ONLY
alias l.='ls -d .* --color=auto'

# LIST ALL FOLDERS
alias lf='ls -Gl | grep ^d'
alias lsd='ls -Gal | grep ^d'

# LIST ALL FILES AND DIRECTORIES
alias l='ls -al --color'
alias ll='ls -al --color'

# CREATE AND JUMP INTO DIRECTORY
function mcd() {
mkdir -p "$1" && cd "$1";
}

# FIND THE BIGGEST IN A DIRECTORY
alias ds='du -ks *|sort -n'
# ===========================

# MAKE DIFF INTO ~/diffs/
dodiff() {
  if [ ! -d ~/diffs ]; then
      mkdir -p ~/diffs
  fi

  branch=`git rev-parse --abbrev-ref HEAD`
  git diff $1 > ~/diffs/$branch-$1.diff
}

# TERMINAL
alias c='clear'

psgrep() {
  ps axuf | grep -v grep | grep "$@" -i --color=auto;
}

fname() {
  find . -iname "*$@*";
}

# SHOW WHICH COMMANDS YOU USE THE MOST
alias freq='cut -f1 -d" " ~/.bash_history | sort | uniq -c | sort -nr | head -n 30'

# ERASE HISTORY FILE AND EXIT
alias histerase='rm -f $HISTFILE && unset HISTFILE && exit'

# CLEAR JUST CURRENT HISTORY SESSION
alias histclear='unset HISTFILE && exit'

# UBUNTU UPGRADE
alias upgrade='apt-get update && apt-get upgrade && apt-get clean'

# USEFUL ALIAS FOR EXTRACT KNOW ARCHIVES
extract () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1     ;;
      *.tar.gz)    tar xzf $1     ;;
      *.bz2)       bunzip2 $1     ;;
      *.rar)       unrar e $1     ;;
      *.gz)        gunzip $1      ;;
      *.tar)       tar xf $1      ;;
      *.tbz2)      tar xjf $1     ;;
      *.tgz)       tar xzf $1     ;;
      *.zip)       unzip $1       ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1        ;;
      *)     echo "'$1' cannot be extracted via extract()" ;;
      esac
    else
      echo "'$1' is not a valid file"
    fi
}

# CRYPT GIVEN FILE
encrypt(){
  openssl enc -aes-256-cbc -pbkdf2 -salt -in ${1} -out ${1}_enc
}

# DECRYPT GIVEN FILE
decrypt (){
  openssl enc -aes-256-cbc -d -pbkdf2 -in ${1} -out ${1%_enc}
}

# DISPLAY WEATHER FROM ICM
weather(){
  l="http://www.meteo.pl/metco/leg4_pl.png"
  p="http://www.meteo.pl/metco/mgram_pict.php?ntype=2n&row=${2}&col=${3}&lang=pl"
  ( montage -geometry +4+4 ${l} ${p} -| display -title "COAMPS: ${1}" - & ) > /dev/null
}
alias wroclaw='weather Wroclaw 143 75'
alias szczecin='weather Szczecin 123 63'
alias amsterdam='weather Amsterdam 126 14'


# DATABASES
alias psql='psql -U postgres'
alias supsql='sudo -u postgres psql'
alias psql='psql -U postgres -h localhost'
export PGHOST=localhost

psql-create-user ()
{
  UNAME=${1}
  UPASS=${2}
  DBNAME=${3}
  if [ "${UPASS}" == "auto" ]; then
      UPASS=$(genpass|awk '{print $1}');
  fi;
  if [ "x${UNAME}" == "x" -o "x${UPASS}" == "x" -o "x${DBNAME}" == "x" ]; then
      echo -e "Missing required argument! The form is:\npsql-create-user user_name user_password db_name";
      return;
  fi;
  Q="REVOKE ALL PRIVILEGES ON DATABASE ${DBNAME} FROM ${UNAME};"
  Q+="DROP USER ${UNAME};"
  Q+="CREATE USER ${UNAME} WITH PASSWORD '${UPASS}';"
  Q+="CREATE DATABASE ${DBNAME};"
  Q+="GRANT ALL PRIVILEGES ON DATABASE ${DBNAME} to ${UNAME};"
  Q+="\t\l"
  echo ${Q} | sed s/\;/\\n/g
  echo ${Q} | psql -U postgres -h localhost
}

# PYTHON VIRTUALENV
jumpenv(){
  source /home/env/${1}/bin/activate
  cd /home/env/${1}/src
}

cd(){
builtin cd "$@" && ls -al --color
[[ $- == *i* && -f "./bin/activate.sh" && -z $VIRTUAL_ENV ]] && source ./bin/activate.sh
[[ $- == *i* && -f "./bin/activate" && -z $VIRTUAL_ENV ]] && source ./bin/activate
}

# KEYS
alias genpass='tr -dc _#A-Z-a-z-0-9 < /dev/urandom | head -c24 | xargs -r -I{} sh -c "echo -n \"{}    \"  && echo {}|base64 -w0&&echo"'
alias gensshkey='ssh-keygen -t rsa -b 2048'

# SOUND
alias playsound='cvlc --vout none'
alias say='espeak -v en'

# LPCXPRESSO
for LPCPATH in /usr/local/lpcxpresso_*/lpcxpresso/tools/bin/
do
  echo ${PATH}|grep ${LPCPATH} -q || export PATH=${PATH}:${LPCPATH}
done

# ESP FreeRTOS SDK
if [ -d "/home/${USER}/workspace/esp-open-sdk" ]; then
  export PATH=/home/${USER}/workspace/esp-open-sdk/xtensa-lx106-elf/bin:$PATH
  alias xtensa-lx106-elf-gcc="xtensa-lx106-elf-gcc -I/home/${USER}/workspace/esp-open-sdk/sdk/include -L/home/${USER}/workspace/esp-open-sdk/sdk/lib"
fi

if [ -d "/usr/share/esp-open-sdk" ]; then
  export PATH=/usr/share/esp-open-sdk/xtensa-lx106-elf/bin:$PATH
  alias xtensa-lx106-elf-gcc="xtensa-lx106-elf-gcc -I/usr/share/esp-open-sdk/sdk/include -L/usr/share/esp-open-sdk/sdk/lib"
fi

if [ -d "${HOME}/.krew/bin" ]; then
  export PATH=${PATH}:${HOME}/.krew/bin/
else
  echo "Missing kubectl krew plugin. Try to install one!"
  rm -rf /tmp/tmp.*
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}"
  WORK_DIR=$(mktemp -d)
  cd ${WORK_DIR}
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz"
  tar zxvf "${KREW}.tar.gz"
  ./"${KREW}" install krew
  rm -rf ${WORK_DIR}
  export PATH=${PATH}:${HOME}/.krew/bin/
  builtin cd
fi

# USER BIN TO PATH
export PATH=${PATH}:~/bin/
export PATH=${PATH}:${HOME}/.local/bin/

# SERVERS LIST
# read a file with servers entry and set
# aliases to it with scp function
# eg: hostname_ssh and hostname_scp()
if [ -f "~/.ssh/know_servers" ];then
  for fs in `cat ~/.ssh/know_servers`
  do
      #hostname:user:ip:home
      N=`echo ${fs}|awk -F':' '{print $1}'`
      U=`echo ${fs}|awk -F':' '{print $2}'`
      I=`echo ${fs}|awk -F':' '{print $3}'`
      H=`echo ${fs}|awk -F':' '{print $4}'`
      if [ "${H}X" == "X" -o "${H}" == '~' ];then
          alias ${N}_ssh="ssh ${U}@${I}"
          eval "function ${N}_scp(){ scp \${1} ${U}@${I}:~/; }"
      else
          alias ${N}_ssh="ssh ${U}@${I} \"cd ${H}; bash --login\""
          eval "function ${N}_scp(){ scp \${1} ${U}@${I}:${H}/; }"
      fi
  done
fi

# SHARE INTERNET
# accept two optional argument,
# interface name and his ip.
share_internet() {
  OUT_IP=${2};OUT_IF=${1};S=`which sudo`
  [ "${OUT_IP}x" == "x" ] && OUT_IP='192.168.0.1'
  [ "${OUT_IF}x" == "x" ] && OUT_IF=`grep rndis /sys/class/net/*/device/uevent|cut -d'/' -f 5`
  ${S} ifconfig ${OUT_IF} down
  ${S} ifconfig ${OUT_IF} ${OUT_IP} netmask 255.255.255.0 up
  GATE_IP=`ip -4 route list 0/0 | cut -d' ' -f 3`
  GATE_IF=`ip -4 route list 0/0 | cut -d' ' -f 5`
  RANGE=`echo ${OUT_IP}|awk -F '.' '{print $1"."$2"."$3"."$4+1","$1"."$2"."$3"."254}'`
  ${S} sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'
  ${S} iptables -t nat -A POSTROUTING -o ${GATE_IF} -j MASQUERADE
  ${S} iptables -A FORWARD -i ${OUT_IF} -j ACCEPT
  ${S} iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
  ${S} iptables -t nat -A POSTROUTING -j MASQUERADE
  ${S} killall -q -9 dnsmasq
  ${S} dnsmasq               \
  --port=0                   \
  --no-daemon                \
  --interface=${OUT_IF}      \
  --bind-interfaces          \
  --listen-address=${OUT_IP} \
  --dhcp-range=${RANGE},7d   \
  --dhcp-option=option:router,${OUT_IP} \
  --dhcp-option=option:dns-server,8.8.4.4 \
  --dhcp-boot=ipxe.pxe \
  --enable-tftp \
  --tftp-root=`pwd`
}

# Similar to above but for already configured interface.
# OUT_IF is a lan side interface, so not that who have internet.
share_internet_no_dhcp () {
    OUT_IF=${1};
    S=`which sudo`;
    GATE_IP=`ip -4 route list 0/0 | cut -d' ' -f 3`;
    GATE_IF=`ip -4 route list 0/0 | cut -d' ' -f 5`;
    ${S} sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forward';
    ${S} iptables -t nat -A POSTROUTING -o ${GATE_IF} -j MASQUERADE;
    ${S} iptables -A FORWARD -i ${OUT_IF} -j ACCEPT;
    ${S} iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT;
    ${S} iptables -t nat -A POSTROUTING -j MASQUERADE;
}

vagrant-up-all(){
  for vm in ${1}*
  do
    if [ -f "${vm}/Vagrantfile" ];then
      cd ${vm}
      vagrant up
      cd ..
    fi
  done
}

vagrant-create(){
  for IP in $(seq ${2} ${3});
  do
    rm -rf ${1}_${IP}
    mkdir -p ${1}_${IP}
    cd ${1}_${IP}
    rm -f Vagrantfile
    vagrant init ubuntu/trusty64
    sed -i s/'^end'//g Vagrantfile
    echo "config.vm.provider \"virtualbox\" do |v|" >> Vagrantfile
    echo "  v.name = \"${1}_${IP}\"" >> Vagrantfile
    echo "end" >> Vagrantfile
    echo "  config.ssh.shell = \"bash -c 'BASH_ENV=/etc/profile exec bash'\"" >> Vagrantfile
    echo "  config.vm.network \"private_network\", ip: \"192.168.56.${IP}\"" >> Vagrantfile
    #echo "  config.vm.provider name: \"${1}_${IP}\"" >> Vagrantfile
    echo "  config.vm.provision \"file\", source: \"~/.ssh/id_rsa.pub\", destination:\"/tmp/key\"" >> Vagrantfile
    echo "  config.vm.provision \"shell\", inline: \"cat /tmp/key >> /home/vagrant/.ssh/authorized_keys && rm /tmp/key\"" >> Vagrantfile
    echo "end" >> Vagrantfile
    vagrant up --provider virtualbox
    cd ..
  done
}

vagrant-destroy(){
  for IP in $(seq ${2} ${3});
  do
    cd ${1}_${IP}
    vagrant destroy -f
    cd ..
    rm -rf ${1}_${IP}
  done
}

hide_desktop_volumes(){
  dconf write /org/mate/caja/desktop/volumes-visible false
}

compress_pdf(){
  # Based on:
  #  https://askubuntu.com/questions/113544/how-can-i-reduce-the-file-size-of-a-scanned-pdf-file
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile="compressed-${1}" "${1}"
}

ch341eeprom(){
  if [ ! -x /usr/local/bin/ch341eeprom ];then
    echo "Missing ch341eeprom binary. Try to build one!"
    rm -rf /tmp/tmp.*
    WORK_DIR=`mktemp -d -p "/tmp/"`
    git clone git@github.com:command-tab/ch341eeprom.git ${WORK_DIR}/ch341eeprom
    cd ${WORK_DIR}/ch341eeprom
    make
    sudo cp -rp ch341eeprom /usr/local/bin/ch341eeprom
    rm -rf ${WORK_DIR}
  else
    /usr/local/bin/ch341eeprom $@
  fi
}

# COMMAND PROMPT
export PS1='\[$red\]\u\[$reset\]@\[$green\]\h\[$reset\]:\[$blue\]\w\[$reset\]\$ '
