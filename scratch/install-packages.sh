#!/bin/bash

PACKAGES="sublime-text vim htop hexchat vlc terminator gtkterm docker-compose docker.io keepass2 xdotool ca-certificates apt-transport-https lsb-release gnupg azure-cli"

# sudo snap install teams-for-linux --edge

azure-cli-init(){
    wget -q https://packages.microsoft.com/keys/microsoft.asc -O - | \
    gpg --dearmor | \
    sudo tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null
    AZ_REPO=$(lsb_release -cs)
    if [ "${AZ_REPO}" == "tina" ];then
      AZ_REPO=bionic
    fi
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | \
    sudo tee /etc/apt/sources.list.d/azure-cli.list
}

sublime-text-init(){
    wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
    sudo apt-get install apt-transport-https
    echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
}

sublime-text-end(){
    echo "..."
}

docker-io-end(){
    sudo groupadd docker
    sudo usermod -aG docker $USER
    newgrp docker
}

install-packages(){
    for package in ${PACKAGES};
    do
	# package=${package}
        if [ "$(type -t ${package}-init)" == "function" ];then
            echo "Run: ${package}-init"
            ${package}-init
        fi
    done
    sudo apt-get update
    sudo apt-get install ${PACKAGES}
    for package in ${PACKAGES};
    do
        # package=${package}
        if [ "$(type -t ${package}-end)" == "function" ];then
            echo "Run: ${package}-end"
            ${package}-end
        fi
    done
}

install-packages
