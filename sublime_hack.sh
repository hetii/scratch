#!/bin/bash

BIN=sublime_text

if [ -f "${BIN}" ]; then
    if grep -obUaPq "\x33\x42" ${BIN}; then
        echo "Patching ${BIN}..."
        sed -i 's/\x33\x42/\x32\x42/g' ${BIN}
    for p in ~/.config/sublime-text-*/Settings/; do
        echo "Installing license into: ${p}"
        cat << EOF > ${p}/License.sublime_license
—–BEGIN LICENSE—–
Patrick Carey
Unlimited User License
EA7E-18848
4982D83B6313800EBD801600D7E3CC13
F2CD59825E2B4C4A18490C5815DF68D6
A5EFCC8698CFE589E105EA829C5273C0
C5744F0857FAD2169C88620898C3845A
1F4521CFC160EEC7A9B382DE605C2E6D
DE84CD0160666D30AA8A0C5492D90BB2
75DEFB9FD0275389F74A59BB0CA2B4EF
EA91E646C7F2A688276BCF18E971E372
—–END LICENSE—–
EOF
        done 
    else
		echo "File ${BIN} is already patched - exit!"
	fi
else
	echo "File ${BIN} is missing - please run this script from Sublime directory."
fi
