#!/bin/bash
set -e

if [[ $(id -u) -ne 0 ]];then
    echo "Please run as root"
    exit 1
fi

virtualbox_trusty(){
    apt-get install dkms
    wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | apt-key add -
    #sh -c 'echo "deb http://download.virtualbox.org/virtualbox/debian trusty contrib" >> /etc/apt/sources.list.d/virtualbox.list'
    apt-get update
    apt-get install virtualbox-4.3
    apt-get install vagrant
}

help(){
    echo Commands are:
    echo "\tmake all\t - execute all below commands"
    echo "\tmake tmp_erase\t - delete all data in /tmp/"
    echo "\tmake tmp_to_ram\t - mount /tmp/ as tmpfs to ram (set that in fstab)"
    echo "\tmake swappiness\t - disable swap by set vm.swappiness = 0"
    echo "\tmake aliases\t - install .bash_aliases with handy methods"
    echo "\tmake apt\t - install few basic tools (please check inside)"
    echo "\tmake motd\t - install motdstat as welcome day message" 
}

create_pendrive(){
    if [ "${1}X" == "X" ];then
        REMOVABLE_DRIVES=""
        for _device in /sys/block/*/device; do
            if echo $(readlink -f "${_device}")|egrep -q "usb"; then
                _disk=$(echo "${_device}" | cut -f4 -d/)
                REMOVABLE_DRIVES="${REMOVABLE_DRIVES} ${_disk}"
            fi
        done

        if [ "${REMOVABLE_DRIVES}X" == "X" ];then
            echo Not found removable drive!
            read -p "Plase insert removable device and type his device name: " DEV
        else
            echo Removable drives found: "${REMOVABLE_DRIVES}"
            DEV=/dev/${REMOVABLE_DRIVES//[[:blank:]]/}
        fi

        if [ `echo ${REMOVABLE_DRIVES}|wc -w` -gt 1 ];then 
            read -p "Plase type with one of your removable device you want to use: " DEV
        fi
    else
        DEV=${1}
    fi 

    if [ "`basename ${DEV}`" == "mmcblk0" ];then
        PART=${DEV}p1
    else
        PART=${DEV}1
    fi
    STICK_DIR=${HOME}/tmp/pendrive
    mount | grep ${DEV} | awk '{ print $1 }' | xargs -r umount
    echo -en  "o\nn\np\n\n\n\nt\n83\na\np\nw" | sudo fdisk ${DEV}
    partprobe
    sudo mkfs.ext2 -F -F -L 'casper-rw' ${PART}
    mkdir -p ${STICK_DIR}
    mount ${PART} ${STICK_DIR}
    dd conv=notrunc bs=440 count=1 if=./mbr.bin of=${DEV}
    ./extlinux --install ${STICK_DIR}
    CFG=${STICK_DIR}/extlinux.conf
    echo "default live" > ${CFG}
    echo "label live" >> ${CFG}
    echo "kernel /casper/vmlinuz.efi" >> ${CFG}
    echo "append  file=/cdrom/preseed/ubuntu-mate.seed ignore_uuid toram boot=casper initrd=/casper/initrd.lz" >> ${CFG}
    sync
    cp -rp ${HOME}/tmp/remaster-iso/casper ${STICK_DIR}
    sync && sync
    umount ${PART}
}

#http://www.syslinux.org/wiki/index.php?title=HowTos
remaster-iso(){
    #export HOME=/root
    apt-get install -y uck
    if [ -f "${2}" ]; then
        uck-remaster-clean
        uck-remaster-unpack-iso ${2}
        uck-remaster-unpack-rootfs        
        echo "====> chroot part1"
        uck-remaster-chroot-rootfs ${HOME}/tmp apt-get install -y git
        echo "====> chroot part2"
        uck-remaster-chroot-rootfs ${HOME}/tmp git clone https://bitbucket.org/hetii/scratch
        echo "====> time to install scratch"
        uck-remaster-chroot-rootfs ${HOME}/tmp /scratch/start.sh all
        echo "====> its pack time"
        uck-remaster-pack-rootfs -c
        uck-remaster-remove-win32-files
        echo "====> create pendrive"
        create_pendrive
        #uck-remaster-pack-iso ubuntu-mate.iso -h -g -d "ubuntu-mate-cd"

        #./bios/linux/syslinux -i /dev/sdb1
        #dd conv=notrunc bs=440 count=1 if=./bios/mbr/mbr.bin of=/dev/sdb
        #parted /dev/sdb set 1 boot on
        #/media/hetii/system/syslinux.cfg
        #echo "default live" > ${CFG}
        #echo "label live" >> ${CFG}
        #echo "kernel /casper/vmlinuz.efi" >> ${CFG}
        #echo "append  file=/cdrom/preseed/ubuntu-mate.seed ignore_uuid toram boot=casper initrd=/casper/initrd.lz" >> ${CFG}
    else
      echo "Missing iso file - exit!"
    fi
}

all(){
    if [ -d "/scratch" ];then
        export HOME=/root
    fi
    tmp_erase
    tmp_to_ram
    swappiness 
    aliases 
    apt 
    #hp1020 
    #motd
    if [ -d "/scratch" ];then
        cp /scratch/.bash_aliases /etc/skel/
        apt-get -y autoremove
        apt-get -y autoclean
    fi
}

tmp_erase(){
    echo erase all data in /tmp/ && rm -rf /tmp/*
}

tmp_to_ram(){
    grep -v '^$\|^\s*\#' /etc/fstab|grep -q '/tmp' && echo '/tmp is already defined in /etc/fstab' || echo "tmpfs /tmp tmpfs mode=1777,nosuid,nodev 0 0" >> /etc/fstab 
    mountpoint -q /tmp || mount /tmp
}

swappiness(){
    echo set swappiness to 0
    grep '^vm.swappiness = 0' -q /etc/sysctl.conf || echo "vm.swappiness = 0" >> /etc/sysctl.conf
}

aliases(){
    test -f .bash_aliases && echo copy .bash_aliases to ~ && cp .bash_aliases ~/
    grep -q '. ~/.bash_aliases' ~/.bashrc || echo '. ~/.bash_aliases' >> ~/.bashrc
}

apt(){
    add-apt-repository --yes ppa:js-reynaud/kicad-4
    apt-get update
    apt-get install -y kicad kicad-library
    apt-get install -y vim htop python-dev ipython python-pip python-virtualenv
    apt-get install -y virtualbox-dkms virtualbox-qt
    apt-get install -y gimp docker.io wireshark unetbootin gtkterm
    apt-get purge -y ubiquity rhythmbox galculator
    rm -rf /usr/share/rhythmbox /usr/lib/rhythmbox
}

motd(){
    apt-get install -y fortune
    git clone https://hetii@bitbucket.org/hetii/motdstat.git /tmp/motdstat
    cd /tmp/motdstat && make install
    rm -rf /tmp/motdstat
}

save(){
    git add * .bash_aliases && git commit && git push
}

# https://mark911.wordpress.com/2014/10/31/how-to-install-printer-drivers-for-hp-laserjet-1020-in-ubuntu-14-04-lts-without-needing-access-to-openprinting-org-website/
hp1020(){
    apt-get update
    apt-get remove hplip cups-filters cups hplip-data system-config-printer-udev -y
    apt-get install build-essential tix groff dc cups -y 
    apt-get install cups-filters unp system-config-printer-gnome -y 
    rm -rf /usr/share/hplip /tmp/foo2zjs
    wget http://foo2zjs.rkkda.com/foo2zjs.tar.gz -O /tmp/foo2zjs.tar.gz
    tar -xzf /tmp/foo2zjs.tar.gz -C /tmp/
    make -C /tmp/foo2zjs
    /tmp/foo2zjs/getweb 1020
    mv sihp1020.img /tmp/foo2zjs
    make -C /tmp/foo2zjs install
    make -C /tmp/foo2zjs install-hotplug
    rm -rf /tmp/foo2zjs
    echo 'Unplug and re-plug the USB printer into the PC add new HP Laserjet 1020 printer via system-config-printer tool and choose to use foo2zjs foomatic printer driver'
    echo "In trouble execute: sudo /etc/hotplug/usb/hplj1020"
    #system-config-printer
}

install_packages(){
  sudo apt install git git-remote-gcrypt curl vim terminator gimp gparted
  sudo apt install keepassx
}

${1} $@
