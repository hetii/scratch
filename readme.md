What is SCRATCH ?
============

`scratch` is a collection of function and aliases that are very useful for anybody who play with linux console.

Usage
=====

Please call `make` for more info. 


        make all         - execute all below commands
        make tmp_erase   - delete all data in /tmp/
        make tmp_to_ram  - mount /tmp/ as tmpfs to ram (set that in fstab)
        make swappiness  - disable swap by set vm.swappiness = 0
        make aliases     - install .bash_aliases with handy methods
        make apt         - install few basic tools (please check inside)
        make motd        - install motdstat as welcome day message
        make save        - save changes to main repository