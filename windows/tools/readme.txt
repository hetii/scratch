## AAct - KMS-activator for operating systems Windows VL editions: 
Vista, 7, 8, 8.1, 10, Server 2008, 2008 R2, 2012, 2012 R2, 2016 and Office 2010, 2013, 2016. 
Also, you can activate Office 2010 VL on Windows XP. 

Additional startup parameters (keys):
/win=act 	- Run program in hidden mode, activate Windows and exit the program.
/ofs=act 	- Run program in hidden mode, activate Office and exit the program.
/wingvlk	- Run program in hidden mode, install Windows Key and exit the program.
/ofsgvlk	- Run program in hidden mode, install Office Key and exit the program.
/taskwin	- Create reactivation task Windows
/taskofs	- Create reactivation task Office
/tap 		- Use TAP
/hook 		- Use Hook
/auto		- Use WinDivert, Hook and TAP
/ip=host:port	- Use on-line KMS-Service.
/ru | /en	-The program will be launched on Russian (English) interface
